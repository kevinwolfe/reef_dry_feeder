#include <freertos/freertos.h>
#include <freertos/task.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "debug.h"
#include "nvm.h"
#include "utils.h"
#include "application.h"
#include "hardware.h"
#include "wifi.h"

static bool     s_feed_now = false;

static void _application_task(void *Param);

typedef struct
{
  union
  {
    struct
    {
      uint32_t unused          :   8;
      uint32_t roll_count      :   8;
      uint32_t time_offset_min :  16;
    };
    uint32_t raw;
  };
} __attribute__((packed)) feed_schedule_t;

static feed_schedule_t s_feeds[NUM_FEED_SCHEDULES];
static feed_schedule_t s_feeds_update[NUM_FEED_SCHEDULES];
static bool            s_feed_schedule_updated = false;

static uint32_t        s_lifetime_roll_cnt = 0;

static bool            s_next_feed_time_valid = false;
static time_t          s_next_feed_time = 0;
static uint8_t         s_next_feed_cnt = 0;

//-----------------------------------------------------------------------------
static void _application_task(void *Param)
{ 
  const uint32_t task_delay_ms = 100;
  
  s_lifetime_roll_cnt = nvm_get_param_int32(NVM_PARAM_LIFETIME_ROLL_CNT);
  
  print("Feed schedule:\n");
  for (uint8_t idx = 0; idx < NUM_FEED_SCHEDULES; idx++)
  {
    uint32_t sched_raw = nvm_get_param_int32(NVM_PARAM_FEED_SCHED_0 + idx);
    s_feeds[idx] = *(feed_schedule_t*)&sched_raw;
    
    char time_of_day_str[32];
    add_offset_time_of_day_str( time_of_day_str, s_feeds[idx].time_offset_min );
    print(" - Schedule #%i: %s, roll count: %i\n", idx, time_of_day_str, s_feeds[idx].roll_count );
  }

  while(1)
  {
    delay_ms(task_delay_ms);
    
    if ( !s_next_feed_time_valid && wifi_ntp_time_is_set() )
    {
      time_t time_now, time_midnight;
      struct tm tm_next;
      time(&time_now);
      localtime_r(&time_now, &tm_next);
      tm_next.tm_hour = 0;
      tm_next.tm_min = 0;
      tm_next.tm_sec = 0;
      time_midnight = mktime(&tm_next);
      uint32_t curr_seconds_after_midnight = time_now - time_midnight;
      
      uint32_t feed_roll_cnt = 0;
      uint32_t feed_seconds_after_midnight_lowest = (uint32_t)-1;
      for ( uint8_t idx = 0; idx < ARRAY_SIZE( s_feeds ); idx++ )
      {
        uint32_t feed_seconds_after_midnight = s_feeds[idx].time_offset_min * 60;
        feed_seconds_after_midnight += ( feed_seconds_after_midnight < curr_seconds_after_midnight ? 24*60*60 : 0 );
        
        if ( ( s_feeds[idx].roll_count > 0 ) && ( feed_seconds_after_midnight < feed_seconds_after_midnight_lowest ) )
        {
          feed_seconds_after_midnight_lowest = feed_seconds_after_midnight;
          feed_roll_cnt = s_feeds[idx].roll_count;
        }
      }
      
      if ( feed_seconds_after_midnight_lowest != (uint32_t)-1 )
      {       
        tm_next.tm_sec   = feed_seconds_after_midnight_lowest;
        s_next_feed_time = mktime(&tm_next);
        s_next_feed_cnt  = feed_roll_cnt;
        
        char strftime_buf[48];       
        strftime(strftime_buf, sizeof(strftime_buf), "%I:%M %p, %x", &tm_next);
        print("Next Feed Time: %s, Roll count: %i\n", strftime_buf, s_next_feed_cnt );
      }
      
      s_next_feed_time_valid = true;
    }
    
    
    if ( s_next_feed_time_valid )
    {
      time_t time_now;
      time(&time_now);

      if ( ( time_now >= s_next_feed_time ) && s_next_feed_cnt )
      {
        s_feed_now = true;
        s_next_feed_time += 45;
        s_next_feed_cnt--;
        print("Queuing scheduled feed now (%i rolls remaining)\n", s_next_feed_cnt);
      }
      
      if ( s_next_feed_cnt )
      {
      }
      else
      {
        s_next_feed_time_valid = false;
      }
    }

    if ( s_feed_now )
    {
      print("Starting feed sequence\n");
      bool starting_at_home = hardware_motor_in_home_position();

      hardware_motor_on();
      
      // Get it out of the home position
      while ( starting_at_home && hardware_motor_in_home_position() )
      {
        delay_ms(1);
      }
      
      // 100 mSec for a little debounce
      delay_ms(100);

      // Get it back into the home position
      while ( !hardware_motor_in_home_position() )
      {
        delay_ms(1);
      }
      
      // Extra time to square up
      delay_ms(120);
      
      hardware_motor_off();

      s_feed_now = false;
      
      nvm_set_param_int32(NVM_PARAM_LIFETIME_ROLL_CNT, ++s_lifetime_roll_cnt);
      
      print("Feed sequence complete\n");
    }
    
    if ( s_feed_schedule_updated )
    {
      print("Updating feed schedule in NVM\n");
      memcpy( s_feeds, s_feeds_update, sizeof( s_feeds ) );

      for (uint8_t idx = 0; idx < NUM_FEED_SCHEDULES; idx++)
      {
        nvm_set_param_int32(NVM_PARAM_FEED_SCHED_0 + idx, s_feeds[idx].raw);
      }

      s_feed_schedule_updated = false;
      s_next_feed_time_valid  = false;
    }
  }  
}

//-----------------------------------------------------------------------------
void application_feed_request(void)
{
  s_feed_now = true;
}
//-----------------------------------------------------------------------------
static uint16_t _html_add_submit_button( char *p_buffer, char *submit_value, char *button_text )
{
  return sprintf( p_buffer , 
                  "<button type=\"submit\" style=\"margin:5px;\" name=\"Action\" value=\"%s\" />%s</button>", 
                  submit_value,
                  button_text );
}

//-----------------------------------------------------------------------------
char * application_get_html( const char *p_custom_header )
{
  static char buffer[2048] = { 0 };
  char *p_buffer = buffer;
  
  if ( p_custom_header )
  {
    p_buffer += sprintf(p_buffer, "%s", p_custom_header);
  }
  
  p_buffer += sprintf(p_buffer, "<h1>System Info</h1>");
  p_buffer += sprintf(p_buffer, "System Time: %s<br>", get_system_time_str());
  
  p_buffer += sprintf(p_buffer, "Next scheduled feeding: ");
  p_buffer += add_formatted_timestamp( p_buffer, s_next_feed_time );
  p_buffer += sprintf(p_buffer, ", Roll Count: %i<br>", s_next_feed_cnt);  
  
  p_buffer += sprintf(p_buffer, "Up-time: ");
  p_buffer += add_formatted_duration_str( p_buffer, system_uptime_s() );
  p_buffer += sprintf(p_buffer, "<br>");
  
  p_buffer += sprintf(p_buffer, "Lifetime Roll Count: %i<br>", s_lifetime_roll_cnt);  
  
  p_buffer += sprintf(p_buffer, "<form action=\"/\" method=\"post\">");
  p_buffer += _html_add_submit_button( p_buffer, "feed_now", "Feed now (one roll)" );
  p_buffer += sprintf(p_buffer, "</form>");
  
  p_buffer += sprintf(p_buffer, "<h1>Feed Schedules</h1><form action=\"/\" method=\"post\">");
  for ( uint8_t idx = 0; idx < ARRAY_SIZE( s_feeds ); idx++ )
  {
    p_buffer += sprintf( p_buffer, "<label for=\"appt\">Feed #%i:</label>", idx);
    p_buffer += sprintf( p_buffer, "<input type=\"time\" name=\"feed%i_time\" value=\"%02d:%02d\"> ", idx, s_feeds[idx].time_offset_min / 60, s_feeds[idx].time_offset_min % 60);
    p_buffer += sprintf( p_buffer, "Roll cnt:<input name=\"feed%i_cnt\" type=\"number\" value=\"%d\" min=\"0\" max=\"6\"/><br>", idx, s_feeds[idx].roll_count);
  }
  p_buffer += _html_add_submit_button( p_buffer, "update_schedule", "Update Schedules" );
  p_buffer += sprintf(p_buffer, "</form>");
  
  return buffer;
}

//-----------------------------------------------------------------------------
char * application_post_html(const char *p_post_data)
{
  // Example post data: 13%3A30&feed0_cnt=0&feed1_time=03%3A00&feed1_cnt=0&feed2_time=03%3A00&feed2_cnt=0&feed3_time=03%3A00&feed3_cnt=0&feed4_time=03%3A00&feed4_cnt=0&feed5_time=03%3A00&feed5_cnt=3
  char post_data_tmp[256];
  strncpy( post_data_tmp, p_post_data, sizeof( post_data_tmp ) );
  
  char return_html_header[256] = { 0 };
  feed_schedule_t new_feeds[NUM_FEED_SCHEDULES];
  memcpy( s_feeds_update, s_feeds, sizeof( new_feeds ) );
  
  char *tok = post_data_tmp;
  while (tok)
  {
    char *tag   = tok;
    char *value = strchr(tok, '=');
    tok         = strchr(tok, '&');
    
    if ( tok )
    {
     *tok++ = 0;
    }

    if (!tag || !value )
    {
      continue;
    }
    
    *value++ = 0;
    
    // Example: tag = feed3_time    
    if ( !strcmp( tag, "Action" ) && !strcmp( value, "feed_now" ) )
    {
      sprintf( return_html_header, "<meta http-equiv=\"refresh\" content=\"10\"/><h1><b>Manual Feeding In Progress!</b></h1>" );
      application_feed_request();
      continue;
    }
    
    // Break response up even more
    int idx = -1;
    sscanf(tag, "feed%i_%s", &idx, tag);

    // print("Tag: <%s> - Value <%s>\n", tag, value );
    // Tag: 0 - <time> - Value <13:30>
    // Tag: 0 - <cnt> - Value <0>
    if ( ( idx < 0 ) || ( idx > ARRAY_SIZE( s_feeds_update ) ) )
    {
      continue;
    }
    
    if ( !strcmp( tag, "time" ) )
    {
      int hour, min;
      sscanf(value, "%d%%3A%d", &hour, &min);     // %3A = :, as in: 09:30 for 9:30 AM

      s_feeds_update[idx].time_offset_min = ( hour * 60 ) + min;
//      print("%i - Time: %i %i\n", idx, hour, min );
    }
    
    if ( !strcmp( tag, "cnt" ) )
    {
//      print("%i - Cnt: %i\n", idx, atoi( value ) );
      s_feeds_update[idx].roll_count = atoi( value );
    }
  }
  
  if ( memcmp( s_feeds, s_feeds_update, sizeof( s_feeds ) ) )
  {
    s_feed_schedule_updated = true;
    sprintf( return_html_header, "<meta http-equiv=\"refresh\" content=\"10\"/><h1><b>Updated feed schedule</b></h1>" );
  }
  
  // Wait a little to let the application task update the next feed times
  delay_s(2);

  return application_get_html( return_html_header );
}

//-----------------------------------------------------------------------------
void application_init(void)
{
  xTaskCreate( _application_task, "app_task", 8192, NULL, 5, NULL);
}
