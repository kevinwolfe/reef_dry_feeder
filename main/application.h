#ifndef _APPLICATION_H_
#define _APPLICATION_H_

void application_init(void);
void application_feed_request(void);

char * application_get_html( const char *p_custom_header );
char * application_post_html(const char *p_post_data);

#endif