//========= Copyright Valve Corporation ============//

#include <string.h>
#include <stdarg.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>

#include "utils.h"
#include "debug.h"
#include "wifi.h"

#define MSG_QUEUE_DEPTH   16

static void _stdio_task( void *pvParameters );

typedef struct
{
  volatile bool       initialized;
 
  StaticQueue_t       stdout_queue_ctx;
  stdout_msg_t        stdout_queue_buffer[ MSG_QUEUE_DEPTH ];
  QueueHandle_t       stdout_queue;
} stdio_task_context_t;

static stdio_task_context_t s_task = { 0 };
static void _write_stdout( bool print_timestamp, const char *p_msg, va_list args );

//-----------------------------------------------------------------------------
void stdio_task_init( void )
{
    xTaskCreate( _stdio_task,  "stdio_task", 4096, NULL, 0, NULL );
}

//-----------------------------------------------------------------------------
static void _stdio_task( void *pvParameters )
{
  const uint32_t thread_period_ms = 10;
  
  s_task.stdout_queue = xQueueCreateStatic( MSG_QUEUE_DEPTH,
                                            sizeof(stdout_msg_t),
                                            (uint8_t*)s_task.stdout_queue_buffer,
                                            &s_task.stdout_queue_ctx );

  s_task.initialized = true;
  
  stdout_msg_t stdout_msg;

  while ( 1 )
  {   
    if ( xQueueReceive( s_task.stdout_queue, &stdout_msg, 0 ) == pdTRUE )
    {
      wifi_write_stdout_msg( &stdout_msg );
      printf( stdout_msg.text );
    }
    
    delay_ms( thread_period_ms );
  }
}

//-----------------------------------------------------------------------------
void print(const char *p_msg, ...)
{
  va_list args;
  va_start( args, p_msg );
  _write_stdout( true, p_msg, args );
  va_end( args );
}

//-----------------------------------------------------------------------------
void print_no_ts(const char *p_msg, ...)
{
  va_list args;
  va_start( args, p_msg );
  _write_stdout( false, p_msg, args );
  va_end( args );
}

//-----------------------------------------------------------------------------
static void _write_stdout( bool print_timestamp, const char *p_msg, va_list args )
{
  if ( !p_msg )
  {
    return;
  }

  stdout_msg_t msg;

  if ( print_timestamp )
  {
    uint32_t msec = system_uptime_s() * 1000;
    uint32_t hours   = msec / 3600000; msec = msec % 3600000;
    uint32_t minutes = msec / 60000; msec = msec % 60000;
    uint32_t seconds = msec / 1000; msec = msec % 1000;

    snprintf( msg.text, sizeof( msg.text ), "%d:%02d:%02d.%03d: ", hours, minutes, seconds, msec );
  }
  vsnprintf( msg.text + strlen(msg.text), sizeof( msg.text ) - strlen(msg.text), p_msg, args );

  if ( !s_task.initialized || ( xQueueSendToBack( s_task.stdout_queue, &msg, 0 ) == errQUEUE_FULL ) )
  {
    printf( msg.text );
    fflush(stdout);
  }
}
