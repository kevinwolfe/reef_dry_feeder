#ifndef _STDIO_TASK_H
#define _STDIO_TASK_H

#define STDOUT_MSG_MAX_LEN       256

typedef struct
{
  char text[STDOUT_MSG_MAX_LEN];
} stdout_msg_t;

void stdio_task_init( void );

void print( const char *p_msg, ... );
void print_no_ts( const char *p_msg, ... );     // No timestamp option

#endif //_STDIO_TASK_H
