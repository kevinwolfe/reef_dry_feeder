#include "driver/ledc.h"
#include "driver/gpio.h"

#include "utils.h"
#include "hardware.h"

#define PIN_BUTTON                (  0 )
#define PIN_POSITION_INDICATOR    ( 37 )
#define PIN_MOTOR_CTL_A1          ( 13 )
#define PIN_MOTOR_CTL_A2          ( 12 )

static void _set_motor_speed(float speed_pct);
static void _setup_pin_pwm( ledc_timer_t timer, ledc_channel_t channel, uint8_t gpio );

//-----------------------------------------------------------------------------
static void _setup_pin_pwm( ledc_timer_t timer, ledc_channel_t channel, uint8_t gpio )
{
  ledc_channel_config_t ledc_channel = {0};
  ledc_channel.gpio_num = gpio;
  ledc_channel.speed_mode = LEDC_LOW_SPEED_MODE;
  ledc_channel.channel = channel;
  ledc_channel.intr_type = LEDC_INTR_DISABLE;
  ledc_channel.timer_sel = timer;
  ledc_channel.duty = 0;

  ledc_timer_config_t ledc_timer = {0};
  ledc_timer.speed_mode = LEDC_LOW_SPEED_MODE;
  ledc_timer.duty_resolution = LEDC_TIMER_10_BIT;
  ledc_timer.timer_num = timer;
  ledc_timer.freq_hz = 25000;
  
  ledc_channel_config(&ledc_channel);
  ledc_timer_config(&ledc_timer);
}

//-----------------------------------------------------------------------------
static void _set_motor_speed(float speed_pct)
{
  static float last_set_speed = -1;
  if ( speed_pct != last_set_speed )
  {
    printf("Setting motor to %i%%\n", (int)(100 * speed_pct));
    uint32_t pwm_duty = (uint32_t)(speed_pct * ((1 << LEDC_TIMER_10_BIT) - 1));
    ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_2, pwm_duty);
    ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_2);
    last_set_speed = speed_pct;
  }
}

//-----------------------------------------------------------------------------
bool hardware_motor_in_home_position(void)
{
  return gpio_get_level( PIN_POSITION_INDICATOR );
}

//-----------------------------------------------------------------------------
void hardware_motor_on(void)
{
  _set_motor_speed(0.45);
}

//-----------------------------------------------------------------------------
void hardware_motor_off(void)
{
  _set_motor_speed(0);
}

//-----------------------------------------------------------------------------
bool hardware_user_button_pressed(void)
{
  return !gpio_get_level( PIN_BUTTON );
}

//-----------------------------------------------------------------------------
void hardware_init()
{
  gpio_config_t io_conf;
  io_conf.pin_bit_mask = ( 1ULL << PIN_BUTTON ) | ( 1ULL << PIN_POSITION_INDICATOR );
  io_conf.mode = GPIO_MODE_INPUT;
  io_conf.intr_type = GPIO_INTR_DISABLE;
  io_conf.pull_up_en = 1;
  io_conf.pull_down_en = 0;
  gpio_config(&io_conf);
  
  io_conf.pin_bit_mask = ( 1ULL << PIN_MOTOR_CTL_A1 ) | ( 1ULL << PIN_MOTOR_CTL_A2 );
  io_conf.mode = GPIO_MODE_OUTPUT;
  gpio_set_level( PIN_MOTOR_CTL_A1, 0 );
  gpio_set_level( PIN_MOTOR_CTL_A2, 0 );
  gpio_config(&io_conf);
  
  _setup_pin_pwm( LEDC_TIMER_2, LEDC_CHANNEL_2, PIN_MOTOR_CTL_A1 );
  _set_motor_speed(0);
}