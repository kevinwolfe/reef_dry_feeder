#ifndef _HARDWARE_H_
#define _HARDWARE_H_

#include <stdbool.h>

void hardware_init();
bool hardware_user_button_pressed(void);
void hardware_motor_on(void);
void hardware_motor_off(void);
bool hardware_motor_in_home_position(void);

#endif
