#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include <esp_event.h>
#include <esp_system.h>
#include <esp_app_format.h>
#include <esp_netif.h>
#include <esp_eth.h>
#include <mqtt_client.h>

#include "utils.h"
#include "wifi.h"

#define MQTT_REQUEST_TOPIC    "reef/frozen_feeder/requests"

//-----------------------------------------------------------------------------
static bool s_mqtt_subscribed = false;
static bool s_force_mqtt_updates = false;
static esp_mqtt_client_handle_t s_mqtt_client = NULL;

//-----------------------------------------------------------------------------
void force_mqtt_update()
{
  s_force_mqtt_updates = true;
}

//-----------------------------------------------------------------------------
static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;
    switch ((esp_mqtt_event_id_t)event_id)
    {
      case MQTT_EVENT_CONNECTED:
          print( "MQTT Connected to server\n" );
          esp_mqtt_client_subscribe(client, MQTT_REQUEST_TOPIC, 0);
          break;

      case MQTT_EVENT_DISCONNECTED:
          s_mqtt_subscribed = false;
          print( "MQTT Server Disconnect!\n");
          break;

      case MQTT_EVENT_DATA:
          event->data[event->data_len] = 0;
          print("MQTT request message data: %s\n", event->data);
//          if (strcmp( event->data, "skip_feeding") == 0 )
//            skip_next_feedings();

          break;

      case MQTT_EVENT_ERROR:
          print( "MQTT Event Error!\n" );
          break;

      case MQTT_EVENT_SUBSCRIBED:
        s_mqtt_subscribed = true;
        break;
        
      case MQTT_EVENT_UNSUBSCRIBED:
        s_mqtt_subscribed = false;
        break;
      
      case MQTT_EVENT_PUBLISHED:
      default:
        break;
    }
    fflush(stdout);
}



//-----------------------------------------------------------------------------
void mqtt_init()
{
  esp_mqtt_client_config_t mqtt_cfg =
  {
      .uri = "mqtt://mqtt.local",     // 192.168.0.41
  };

  s_mqtt_client = esp_mqtt_client_init(&mqtt_cfg);
  esp_mqtt_client_register_event(s_mqtt_client, ESP_EVENT_ANY_ID, mqtt_event_handler, NULL);
  esp_mqtt_client_start(s_mqtt_client);
}

//-----------------------------------------------------------------------------
// Run on the wifi task
void mqtt_do_work()
{
  static float last_status_update = 0;
  static float last_ip_update = 0;
  
  if ( !s_mqtt_client )
  {
    return;
  }
  
  if ( s_force_mqtt_updates )
  {
    last_status_update = 0;
    last_ip_update     = 0;
    s_force_mqtt_updates = false;
  }
  
  uint32_t status_update_rate_s = 5;
  if ( ( last_status_update == 0 ) || ( ( system_uptime_s() - last_status_update ) > status_update_rate_s ) )
  {
    static char status_msg[256];
    static uint32_t status_msg_id = 0;
    
    char *p_msg = status_msg;
    status_msg_id++;
    p_msg += sprintf( p_msg, "{ \"message_id\":%i,",    status_msg_id );
    p_msg += sprintf( p_msg, "\"uptime\":%u,",          (uint32_t)system_uptime_s() );   
    p_msg += sprintf( p_msg, "\"system_time\":\"%s\"",  get_system_time_str() );    
    p_msg += sprintf( p_msg, "}" );
    
    esp_mqtt_client_publish( s_mqtt_client, "reef/dry_feeder/status", status_msg, 0, 1, 0);
    last_status_update = system_uptime_s();
  }
  
  if ( ( last_ip_update == 0 ) || ( ( system_uptime_s() - last_ip_update ) > 60 ) )
  {
    char ip_msg[48];
    sprintf( ip_msg, "{ \"ip_addr\":\"%s\"}", wifi_get_ip_addr_str() );
    esp_mqtt_client_publish( s_mqtt_client, "reef/dry_feeder/ip", ip_msg, 0, 1, 0);
    last_ip_update = system_uptime_s();
  }
}
