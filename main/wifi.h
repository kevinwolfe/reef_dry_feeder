#ifndef _WIFI_H_
#define _WIFI_H_

#include "debug.h"

void wifi_task_init();
void wifi_write_stdout_msg( stdout_msg_t *p_stdout_msg );
void wifi_reset_provisioning(void);
bool wifi_ntp_time_is_set();
const char *wifi_get_ip_addr_str();

#endif
